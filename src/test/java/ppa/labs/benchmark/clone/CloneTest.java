package ppa.labs.benchmark.clone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.Test;

import ppa.labs.benchmark.clone.model.Address;
import ppa.labs.benchmark.clone.model.Authentication;
import ppa.labs.benchmark.clone.model.User;
import ppa.labs.benchmark.clone.model.apache.AddressApa;
import ppa.labs.benchmark.clone.model.apache.AuthenticationApa;
import ppa.labs.benchmark.clone.model.apache.UserApa;

public class CloneTest {

	@Test
	public void cloneUserByJava_shouldBeEquals_Test () {
		Authentication auth		= MockInstance.mockAuth("jdoe", "123456");
		Address addr1	= MockInstance.mockAddress("wall street avenue",
				new Integer(17),
				new BigDecimal(19.15644),
				new BigDecimal(12.6666),
				new Boolean(false));
		Address addr2	= MockInstance.mockAddress("champs elysées avenue",
				new Integer(150),
				new BigDecimal(44.0),
				new BigDecimal(77.87),
				new Boolean(true));
		
		Set<Address> addresses;
		User user;
		User userCloned;
		
		addresses	= new HashSet<>();
		addresses.add(addr1);
		addresses.add(addr2);
		user = MockInstance.mockUser("John", "Doe", auth, addresses);
		userCloned	= (User) user.clone();
		assertEquals(user, userCloned);
	}
	
	@Test
	public void cloneUserByJava_shouldNotBeEquals_Test () {
		Authentication auth		= MockInstance.mockAuth("jdoe", "123456");
		Address addr1	= MockInstance.mockAddress("wall street avenue",
				new Integer(17),
				new BigDecimal(19.15644),
				new BigDecimal(12.6666),
				new Boolean(false));
		Address addr2	= MockInstance.mockAddress("champs elysées avenue",
				new Integer(150),
				new BigDecimal(44.0),
				new BigDecimal(77.87),
				new Boolean(true));
		
		Set<Address> addresses;
		User user;
		User userCloned;
		
		addresses	= new HashSet<>();
		addresses.add(addr1);
		addresses.add(addr2);
		user = MockInstance.mockUser("John", "Doe", auth, addresses);
		userCloned	= (User) user.clone();
		
		userCloned.getAddresses().iterator().next().setNameStreet("totor street");
		
		assertNotEquals(user, userCloned);
	}
	
	@Test
	public void cloneUserByApache_shouldBeEquals_Test () {
		AuthenticationApa auth		= MockInstanceApa.mockAuth("jdoe", "123456");
		AddressApa addr1	= MockInstanceApa.mockAddress("wall street avenue",
				new Integer(17),
				new BigDecimal(19.15644),
				new BigDecimal(12.6666),
				new Boolean(false));
		AddressApa addr2	= MockInstanceApa.mockAddress("champs elysées avenue",
				new Integer(150),
				new BigDecimal(44.0),
				new BigDecimal(77.87),
				new Boolean(true));
		
		Set<AddressApa> addresses;
		UserApa user;
		UserApa userCloned;
		
		addresses	= new HashSet<>();
		addresses.add(addr1);
		addresses.add(addr2);
		user = MockInstanceApa.mockUser("John", "Doe", auth, addresses);
		userCloned	= (UserApa) SerializationUtils.clone(user);
		assertEquals(user, userCloned);
	}
	
	@Test
	public void cloneUserByApache_shouldNotBeEquals_Test () {
		AuthenticationApa auth		= MockInstanceApa.mockAuth("jdoe", "123456");
		AddressApa addr1	= MockInstanceApa.mockAddress("wall street avenue",
				new Integer(17),
				new BigDecimal(19.15644),
				new BigDecimal(12.6666),
				new Boolean(false));
		AddressApa addr2	= MockInstanceApa.mockAddress("champs elysées avenue",
				new Integer(150),
				new BigDecimal(44.0),
				new BigDecimal(77.87),
				new Boolean(true));
		
		Set<AddressApa> addresses;
		UserApa user;
		UserApa userCloned;
		
		addresses	= new HashSet<>();
		addresses.add(addr1);
		addresses.add(addr2);
		user = MockInstanceApa.mockUser("John", "Doe", auth, addresses);
		userCloned	= (UserApa) SerializationUtils.clone(user);
		
		userCloned.getAddresses().iterator().next().setNameStreet("totor street");
		
		assertNotEquals(user, userCloned);
	}
}
