package ppa.labs.benchmark.clone;

import static org.junit.Assert.assertEquals;

import java.util.Set;

import org.junit.Test;

import ppa.labs.benchmark.clone.model.User;

public class MockInstanceTest {

	@Test
	public void mockUsersTest () {
		int nbu	= 10;
		int nba	= 10;
		Set<User> users	= MockInstance.mockUsers(nbu, nba);
		assertEquals(nbu, users.size());
		users.forEach(user -> assertEquals(nba, user.getAddresses().size()));
	}
}
 