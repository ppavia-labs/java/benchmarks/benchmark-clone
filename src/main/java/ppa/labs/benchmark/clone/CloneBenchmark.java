/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package ppa.labs.benchmark.clone;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.SerializationUtils;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import ppa.labs.benchmark.clone.model.User;
import ppa.labs.benchmark.clone.model.apache.UserApa;
import ppa.labs.benchmark.clone.model.constructor.UserConst;
import ppa.labs.benchmark.clone.model.gson.UserGson;
import ppa.labs.benchmark.clone.model.jackson.UserJackson;

@State(Scope.Benchmark)
public class CloneBenchmark {
	@State(Scope.Thread)
	public static class CloneState {
		public static Set<User> users = new HashSet<>();
		public static Set<UserApa> usersApa = new HashSet<>();
		public static Set<UserGson> usersGson = new HashSet<>();
		public static Set<UserJackson> usersJackson = new HashSet<>();
		public static Set<UserConst> usersConst = new HashSet<>();
		@Setup
		public static void setUsers () {
			int nbu	= 10;
			int nba	= 100;
			users	= MockInstance.mockUsers(nbu, nba);
		}
		@Setup
		public static void setUsersApa () {
			int nbu	= 10;
			int nba	= 100;
			usersApa	= MockInstanceApa.mockUsers(nbu, nba);
		}
		
		@Setup
		public static void setUsersGson () {
			int nbu	= 10;
			int nba	= 100;
			usersGson	= MockInstanceGson.mockUsers(nbu, nba);
		}
		
		@Setup
		public static void setUsersJackson () {
			int nbu	= 10;
			int nba	= 100;
			usersJackson	= MockInstanceJackson.mockUsers(nbu, nba);
		}
		
		@Setup
		public static void setUsersConst () {
			int nbu	= 10;
			int nba	= 100;
			usersConst	= MockInstanceConst.mockUsers(nbu, nba);
		}
		
//		@TearDown(Level.Trial)
//        public void doTearDown() {
//            System.out.println("Do TearDown");
//        }
	}
	
	@Benchmark
	public void benchmark_cloneByJava() {
		Set<User> usersCloned = new HashSet<>();
		CloneState.users.forEach(user -> {
			User userCloned = (User) user.clone();
			usersCloned.add(userCloned);
		});
	}
	
	@Benchmark
	public void benchmark_cloneByApache() {
		Set<UserApa> usersCloned = new HashSet<>();
		CloneState.usersApa.forEach(user -> {
			UserApa userCloned = (UserApa) SerializationUtils.clone(user);
			usersCloned.add(userCloned);
		});
	}
	
	@Benchmark
	public void benchmark_cloneByGson() {
		final Gson gson = new Gson();
		Set<UserGson> usersCloned = new HashSet<>();
		CloneState.usersGson.forEach(user -> {
			UserGson userCloned = gson.fromJson(gson.toJson(user), UserGson.class);
			usersCloned.add(userCloned);
		});
	}
	
	@Benchmark
	public void benchmark_cloneByJackson() {
		final ObjectMapper mapper = new ObjectMapper();
		Set<UserJackson> usersCloned = new HashSet<>();
		CloneState.usersJackson.forEach(user -> {
			UserJackson userCloned;
			try {
				userCloned = mapper.readValue(mapper.writeValueAsString(user), UserJackson.class);
				usersCloned.add(userCloned);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		});
	}
	
	@Benchmark
	public void benchmark_cloneByConst() {
		Set<UserConst> usersCloned = new HashSet<>();
		CloneState.usersConst.forEach(user -> {
			UserConst userCloned = user.duplicate();
			usersCloned.add(userCloned);
		});
	}
}
