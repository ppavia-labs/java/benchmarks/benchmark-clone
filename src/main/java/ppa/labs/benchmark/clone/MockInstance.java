package ppa.labs.benchmark.clone;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import ppa.labs.benchmark.clone.model.Address;
import ppa.labs.benchmark.clone.model.Authentication;
import ppa.labs.benchmark.clone.model.User;

public class MockInstance {
	public static User mockUser (String firstName, String lastName, Authentication auth, Set<Address> addresses) {
		User user = new User();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setAuth(auth);
		user.setAddresses(addresses);
		return user;
	}
	
	public static Authentication mockAuth (String login, String pwd) {
		Authentication auth = new Authentication();
		auth.setLogin(login);
		auth.setPassword(pwd);
		return auth;		
	}

	public static Address mockAddress (
			String nameStreet,
			Integer numStreet,
			BigDecimal longitude,
			BigDecimal lattitude,
			Boolean isPrincipal) {
		Address addr = new Address();
		addr.setNameStreet(nameStreet);
		addr.setNumStreet(numStreet);
		addr.setLongitude(longitude);
		addr.setLattitude(lattitude);
		addr.setIsPrincipal(isPrincipal);
		return addr;
	}
	
	public static Set<Address> mockAddresses (int nbAddr) {
		Set<Address> addresses	= new HashSet<>();
		Address addr = new Address();
		String nameStreet		= "";
		Integer numStreet		= 120;
		BigDecimal longitude	= BigDecimal.ZERO;
		BigDecimal lattitude	= BigDecimal.ZERO;
		Boolean isPrincipal		= false;
		while (nbAddr > 0) {
			nameStreet		= String.format("the street [%d]", nbAddr);
			addr = mockAddress(
					nameStreet,
					numStreet+nbAddr,
					longitude.add(new BigDecimal(nbAddr)),
					lattitude.add(new BigDecimal(nbAddr)),
					!isPrincipal);
			addresses.add(addr);
			nbAddr--;

		}
		return addresses;
	}
	
	public static Set<User> mockUsers (int nbUsers, int nbAddrPerUser) {
		Set<User> users	= new HashSet<>();
		User user			= null;
		
		while (nbUsers > 0) {
			String firstName	= String.format("firstname [%d]", nbUsers);
			String lastName		= String.format("lastname [%d]", nbUsers);
			String login		= String.format("login [%d]", nbUsers);
			String pwd			= "" +
					LocalDateTime.now() + 
					Math.random()
					;
			user = mockUser(
					firstName,
					lastName,
					mockAuth(login, pwd),
					mockAddresses(nbAddrPerUser));
			users.add(user);
			nbUsers--;

		}
		
		return users;
	}
}
 