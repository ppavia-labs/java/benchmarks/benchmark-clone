package ppa.labs.benchmark.clone;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import ppa.labs.benchmark.clone.model.constructor.AddressConst;
import ppa.labs.benchmark.clone.model.constructor.AuthenticationConst;
import ppa.labs.benchmark.clone.model.constructor.UserConst;


public class MockInstanceConst {
	public static UserConst mockUser (String firstName, String lastName, AuthenticationConst auth, Set<AddressConst> addresses) {
		UserConst user = new UserConst();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setAuth(auth);
		user.setAddresses(addresses);
		return user;
	}
	
	public static AuthenticationConst mockAuth (String login, String pwd) {
		AuthenticationConst auth = new AuthenticationConst();
		auth.setLogin(login);
		auth.setPassword(pwd);
		return auth;		
	}

	public static AddressConst mockAddress (
			String nameStreet,
			Integer numStreet,
			BigDecimal longitude,
			BigDecimal lattitude,
			Boolean isPrincipal) {
		AddressConst addr = new AddressConst();
		addr.setNameStreet(nameStreet);
		addr.setNumStreet(numStreet);
		addr.setLongitude(longitude);
		addr.setLattitude(lattitude);
		addr.setIsPrincipal(isPrincipal);
		return addr;
	}
	
	public static Set<AddressConst> mockAddresses (int nbAddr) {
		Set<AddressConst> addresses	= new HashSet<>();
		AddressConst addr = new AddressConst();
		String nameStreet		= "";
		Integer numStreet		= 120;
		BigDecimal longitude	= BigDecimal.ZERO;
		BigDecimal lattitude	= BigDecimal.ZERO;
		Boolean isPrincipal		= false;
		while (nbAddr > 0) {
			nameStreet		= String.format("the street [%d]", nbAddr);
			addr = mockAddress(
					nameStreet,
					numStreet+nbAddr,
					longitude.add(new BigDecimal(nbAddr)),
					lattitude.add(new BigDecimal(nbAddr)),
					!isPrincipal);
			addresses.add(addr);
			nbAddr--;

		}
		return addresses;
	}
	
	public static Set<UserConst> mockUsers (int nbUsers, int nbAddrPerUser) {
		Set<UserConst> users	= new HashSet<>();
		UserConst user			= null;
		
		while (nbUsers > 0) {
			String firstName	= String.format("firstname [%d]", nbUsers);
			String lastName		= String.format("lastname [%d]", nbUsers);
			String login		= String.format("login [%d]", nbUsers);
			String pwd			= "" +
					LocalDateTime.now() + 
					Math.random()
					;
			user = mockUser(
					firstName,
					lastName,
					mockAuth(login, pwd),
					mockAddresses(nbAddrPerUser));
			users.add(user);
			nbUsers--;

		}
		
		return users;
	}
}
 