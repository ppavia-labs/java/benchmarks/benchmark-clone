package ppa.labs.benchmark.clone;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import ppa.labs.benchmark.clone.model.gson.AddressGson;
import ppa.labs.benchmark.clone.model.gson.AuthenticationGson;
import ppa.labs.benchmark.clone.model.gson.UserGson;

public class MockInstanceGson {
	public static UserGson mockUser (String firstName, String lastName, AuthenticationGson auth, Set<AddressGson> addresses) {
		UserGson user = new UserGson();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setAuth(auth);
		user.setAddresses(addresses);
		return user;
	}
	
	public static AuthenticationGson mockAuth (String login, String pwd) {
		AuthenticationGson auth = new AuthenticationGson();
		auth.setLogin(login);
		auth.setPassword(pwd);
		return auth;		
	}

	public static AddressGson mockAddress (
			String nameStreet,
			Integer numStreet,
			BigDecimal longitude,
			BigDecimal lattitude,
			Boolean isPrincipal) {
		AddressGson addr = new AddressGson();
		addr.setNameStreet(nameStreet);
		addr.setNumStreet(numStreet);
		addr.setLongitude(longitude);
		addr.setLattitude(lattitude);
		addr.setIsPrincipal(isPrincipal);
		return addr;
	}
	
	public static Set<AddressGson> mockAddresses (int nbAddr) {
		Set<AddressGson> addresses	= new HashSet<>();
		AddressGson addr = new AddressGson();
		String nameStreet		= "";
		Integer numStreet		= 120;
		BigDecimal longitude	= BigDecimal.ZERO;
		BigDecimal lattitude	= BigDecimal.ZERO;
		Boolean isPrincipal		= false;
		while (nbAddr > 0) {
			nameStreet		= String.format("the street [%d]", nbAddr);
			addr = mockAddress(
					nameStreet,
					numStreet+nbAddr,
					longitude.add(new BigDecimal(nbAddr)),
					lattitude.add(new BigDecimal(nbAddr)),
					!isPrincipal);
			addresses.add(addr);
			nbAddr--;

		}
		return addresses;
	}
	
	public static Set<UserGson> mockUsers (int nbUsers, int nbAddrPerUser) {
		Set<UserGson> users	= new HashSet<>();
		UserGson user			= null;
		
		while (nbUsers > 0) {
			String firstName	= String.format("firstname [%d]", nbUsers);
			String lastName		= String.format("lastname [%d]", nbUsers);
			String login		= String.format("login [%d]", nbUsers);
			String pwd			= "" +
					LocalDateTime.now() + 
					Math.random()
					;
			user = mockUser(
					firstName,
					lastName,
					mockAuth(login, pwd),
					mockAddresses(nbAddrPerUser));
			users.add(user);
			nbUsers--;

		}
		
		return users;
	}
}
 