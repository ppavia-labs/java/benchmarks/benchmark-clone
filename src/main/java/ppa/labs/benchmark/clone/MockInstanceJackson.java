package ppa.labs.benchmark.clone;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import ppa.labs.benchmark.clone.model.jackson.AddressJackson;
import ppa.labs.benchmark.clone.model.jackson.AuthenticationJackson;
import ppa.labs.benchmark.clone.model.jackson.UserJackson;

public class MockInstanceJackson {
	public static UserJackson mockUser (String firstName, String lastName, AuthenticationJackson auth, Set<AddressJackson> addresses) {
		UserJackson user = new UserJackson();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setAuth(auth);
		user.setAddresses(addresses);
		return user;
	}
	
	public static AuthenticationJackson mockAuth (String login, String pwd) {
		AuthenticationJackson auth = new AuthenticationJackson();
		auth.setLogin(login);
		auth.setPassword(pwd);
		return auth;		
	}

	public static AddressJackson mockAddress (
			String nameStreet,
			Integer numStreet,
			BigDecimal longitude,
			BigDecimal lattitude,
			Boolean isPrincipal) {
		AddressJackson addr = new AddressJackson();
		addr.setNameStreet(nameStreet);
		addr.setNumStreet(numStreet);
		addr.setLongitude(longitude);
		addr.setLattitude(lattitude);
		addr.setIsPrincipal(isPrincipal);
		return addr;
	}
	
	public static Set<AddressJackson> mockAddresses (int nbAddr) {
		Set<AddressJackson> addresses	= new HashSet<>();
		AddressJackson addr = new AddressJackson();
		String nameStreet		= "";
		Integer numStreet		= 120;
		BigDecimal longitude	= BigDecimal.ZERO;
		BigDecimal lattitude	= BigDecimal.ZERO;
		Boolean isPrincipal		= false;
		while (nbAddr > 0) {
			nameStreet		= String.format("the street [%d]", nbAddr);
			addr = mockAddress(
					nameStreet,
					numStreet+nbAddr,
					longitude.add(new BigDecimal(nbAddr)),
					lattitude.add(new BigDecimal(nbAddr)),
					!isPrincipal);
			addresses.add(addr);
			nbAddr--;

		}
		return addresses;
	}
	
	public static Set<UserJackson> mockUsers (int nbUsers, int nbAddrPerUser) {
		Set<UserJackson> users	= new HashSet<>();
		UserJackson user			= null;
		
		while (nbUsers > 0) {
			String firstName	= String.format("firstname [%d]", nbUsers);
			String lastName		= String.format("lastname [%d]", nbUsers);
			String login		= String.format("login [%d]", nbUsers);
			String pwd			= "" +
					LocalDateTime.now() + 
					Math.random()
					;
			user = mockUser(
					firstName,
					lastName,
					mockAuth(login, pwd),
					mockAddresses(nbAddrPerUser));
			users.add(user);
			nbUsers--;

		}
		
		return users;
	}
}
 