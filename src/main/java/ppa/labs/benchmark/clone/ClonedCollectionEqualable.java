package ppa.labs.benchmark.clone;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public interface ClonedCollectionEqualable<T> {
	
	default boolean isEqualMembersInCollection(Collection<T> ls1, Collection<T> ls2) {
        Set<Boolean> eq = ls1.stream().map(ob1 -> {
            return ls2.stream().anyMatch(ob2 -> ob1.equals(ob2));
        }).filter(isEq -> isEq == false).collect(Collectors.toSet());
        if(eq.size() > 0) {
            return false;
        } else {
            return true;
        }
    }

    default boolean equalMembers(Collection<T> member, Collection<T> o) {
        if((member == null && o != null) || (member != null && o == null)) {
            return false;
        }

        if(member == null && o == null) {
            return true;
        }

        if(member.size() != o.size()) {
            return false;
        }

        if(member.equals(o)) {
            return true;
        }

        return isEqualMembersInCollection(member, o);
    }
}
