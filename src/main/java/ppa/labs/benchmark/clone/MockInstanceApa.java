package ppa.labs.benchmark.clone;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import ppa.labs.benchmark.clone.model.apache.AddressApa;
import ppa.labs.benchmark.clone.model.apache.AuthenticationApa;
import ppa.labs.benchmark.clone.model.apache.UserApa;

public class MockInstanceApa {
	public static UserApa mockUser (String firstName, String lastName, AuthenticationApa auth, Set<AddressApa> addresses) {
		UserApa user = new UserApa();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setAuth(auth);
		user.setAddresses(addresses);
		return user;
	}
	
	public static AuthenticationApa mockAuth (String login, String pwd) {
		AuthenticationApa auth = new AuthenticationApa();
		auth.setLogin(login);
		auth.setPassword(pwd);
		return auth;		
	}

	public static AddressApa mockAddress (
			String nameStreet,
			Integer numStreet,
			BigDecimal longitude,
			BigDecimal lattitude,
			Boolean isPrincipal) {
		AddressApa addr = new AddressApa();
		addr.setNameStreet(nameStreet);
		addr.setNumStreet(numStreet);
		addr.setLongitude(longitude);
		addr.setLattitude(lattitude);
		addr.setIsPrincipal(isPrincipal);
		return addr;
	}
	
	public static Set<AddressApa> mockAddresses (int nbAddr) {
		Set<AddressApa> addresses	= new HashSet<>();
		AddressApa addr = new AddressApa();
		String nameStreet		= "";
		Integer numStreet		= 120;
		BigDecimal longitude	= BigDecimal.ZERO;
		BigDecimal lattitude	= BigDecimal.ZERO;
		Boolean isPrincipal		= false;
		while (nbAddr > 0) {
			nameStreet		= String.format("the street [%d]", nbAddr);
			addr = mockAddress(
					nameStreet,
					numStreet+nbAddr,
					longitude.add(new BigDecimal(nbAddr)),
					lattitude.add(new BigDecimal(nbAddr)),
					!isPrincipal);
			addresses.add(addr);
			nbAddr--;

		}
		return addresses;
	}
	
	public static Set<UserApa> mockUsers (int nbUsers, int nbAddrPerUser) {
		Set<UserApa> users	= new HashSet<>();
		UserApa user			= null;
		
		while (nbUsers > 0) {
			String firstName	= String.format("firstname [%d]", nbUsers);
			String lastName		= String.format("lastname [%d]", nbUsers);
			String login		= String.format("login [%d]", nbUsers);
			String pwd			= "" +
					LocalDateTime.now() + 
					Math.random()
					;
			user = mockUser(
					firstName,
					lastName,
					mockAuth(login, pwd),
					mockAddresses(nbAddrPerUser));
			users.add(user);
			nbUsers--;

		}
		
		return users;
	}
}
 