package ppa.labs.benchmark.clone.model.apache;

import java.io.Serializable;

public class AuthenticationApa implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4931757635016926649L;
	private String login;
	private String password;
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString () {
		return (String.format(
				"\n----- obj = %s\n" +
						"login = %s\n" +
						"password = %s\n",
						super.toString(),
						this.getLogin(),
						this.getPassword()
				));
	}
	
	@Override
	public boolean equals(Object o) { 
  
		if (o == this) { 
			return true;
		}

		/* Check if o is an instance of Authentication or not 
          "null instanceof [type]" also returns false */
		if (!(o instanceof AuthenticationApa)) { 
			return false; 
		} 

		// typecast o to Authentication so that we can compare data members
		AuthenticationApa c = (AuthenticationApa) o; 

		// Compare the data members and return accordingly  
		return this.getLogin().equals(c.getLogin()) &&
				this.getPassword().equals(c.getPassword()); 
	}
}
