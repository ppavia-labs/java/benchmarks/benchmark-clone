package ppa.labs.benchmark.clone.model.jackson;

import java.util.Set;

public class UserJackson {

	private String lastName;
	private String firstName;
	private AuthenticationJackson auth;
	private Set<AddressJackson> addresses;
	
	public UserJackson() {}
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public AuthenticationJackson getAuth() {
		return auth;
	}
	public void setAuth(AuthenticationJackson auth) {
		this.auth = auth;
	}	
	public Set<AddressJackson> getAddresses() {
		return addresses;
	}
	public void setAddresses(Set<AddressJackson> addresses) {
		this.addresses = addresses;
	}

	@Override
	public String toString () {
		return (String.format(
				"\n----- obj = %s\n" +
						"lastName = %s\n" +
						"firstName = %s\n" +
						"auth = %s\n" +
						"addresses = %s\n",
						super.toString(),
						this.getLastName(),
						this.getFirstName(),
						this.getAuth().toString(),
						this.addressesToString()
				));
	}
	
	public String addressesToString () {
		final StringBuilder out	= new StringBuilder("");
		if (this.addresses != null ) {
			this.addresses.stream()
			.forEach(addr -> {
				out.append(addr.toString()).append("\n");
			});
			return out.toString();
		}
		return "null";
	}
}
