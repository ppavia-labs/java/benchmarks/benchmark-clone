package ppa.labs.benchmark.clone.model.constructor;

import java.util.Set;
import java.util.stream.Collectors;

public class UserConst {

	private String lastName;
	private String firstName;
	private AuthenticationConst auth;
	private Set<AddressConst> addresses;
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public AuthenticationConst getAuth() {
		return auth;
	}
	public void setAuth(AuthenticationConst auth) {
		this.auth = auth;
	}	
	public Set<AddressConst> getAddresses() {
		return addresses;
	}
	public void setAddresses(Set<AddressConst> addresses) {
		this.addresses = addresses;
	}

	@Override
	public String toString () {
		return (String.format(
				"\n----- obj = %s\n" +
						"lastName = %s\n" +
						"firstName = %s\n" +
						"auth = %s\n" +
						"addresses = %s\n",
						super.toString(),
						this.getLastName(),
						this.getFirstName(),
						this.getAuth().toString(),
						this.addressesToString()
				));
	}
	
	public String addressesToString () {
		final StringBuilder out	= new StringBuilder("");
		if (this.addresses != null ) {
			this.addresses.stream()
			.forEach(addr -> {
				out.append(addr.toString()).append("\n");
			});
			return out.toString();
		}
		return "null";
	}
	
	public UserConst duplicate () {
		UserConst user	= new UserConst();
		
		user.setAuth(this.auth.duplicate());
		user.setFirstName(new String(this.firstName));
		user.setLastName(new String(this.lastName));
		
		user.addresses = this.addresses.stream()
				.map(addr -> addr.duplicate())
				.collect(Collectors.toSet());
		
		return user;
	}
}
