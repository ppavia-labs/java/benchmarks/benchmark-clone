package ppa.labs.benchmark.clone.model.constructor;

import java.math.BigDecimal;

public class AddressConst {

	private String nameStreet;
	private Integer numStreet;
	private BigDecimal longitude;
	private BigDecimal lattitude;
	private Boolean isPrincipal;
	public String getNameStreet() {
		return nameStreet;
	}
	public void setNameStreet(String nameStreet) {
		this.nameStreet = nameStreet;
	}
	public Integer getNumStreet() {
		return numStreet;
	}
	public void setNumStreet(Integer numStreet) {
		this.numStreet = numStreet;
	}
	public BigDecimal getLongitude() {
		return longitude;
	}
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}
	public BigDecimal getLattitude() {
		return lattitude;
	}
	public void setLattitude(BigDecimal lattitude) {
		this.lattitude = lattitude;
	}
	public Boolean getIsPrincipal() {
		return isPrincipal;
	}
	public void setIsPrincipal(Boolean isPrincipal) {
		this.isPrincipal = isPrincipal;
	}

	@Override
	public String toString () {
		return (String.format(
				"\n----- obj = %s\n" +
						"nameStreet = %s\n" +
						"numStreet = %d\n" +
						"longitude = %f\n" +
						"lattitude = %f\n" +
						"isPrincipal = %s\n",
						super.toString(),
						this.getNameStreet(),
						this.getNumStreet(),
						this.getLongitude(),
						this.getLattitude(),
						String.valueOf(this.getIsPrincipal())
				));
	}
	
	public AddressConst duplicate() {
		AddressConst addr = new AddressConst();
		
		addr.setIsPrincipal(new Boolean(this.isPrincipal));
		addr.setLattitude(this.lattitude);
		addr.setLongitude(this.longitude);
		addr.setNameStreet(new String(this.nameStreet));
		addr.setNumStreet(new Integer(this.numStreet));
		
		return addr;
	}
}
