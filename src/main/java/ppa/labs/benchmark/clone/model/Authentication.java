package ppa.labs.benchmark.clone.model;

public class Authentication implements Cloneable {
	private String login;
	private String password;
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public Object clone() {
		Authentication login = null;
		try {
			return (Authentication) super.clone();
		} catch (CloneNotSupportedException e) {
			login = new Authentication();
			e.printStackTrace();
		}
		return login;
	}
	
	public Object shallowClone() {
		Authentication login = null;
		try {
			return (Authentication) super.clone();
		} catch (CloneNotSupportedException e) {
			login = new Authentication();
			e.printStackTrace();
		}
		return login;
	}

	@Override
	public String toString () {
		return (String.format(
				"\n----- obj = %s\n" +
						"login = %s\n" +
						"password = %s\n",
						super.toString(),
						this.getLogin(),
						this.getPassword()
				));
	}

	@Override
	public boolean equals(Object o) { 
  
		if (o == this) { 
			return true;
		}

		/* Check if o is an instance of Authentication or not 
          "null instanceof [type]" also returns false */
		if (!(o instanceof Authentication)) { 
			return false; 
		} 

		// typecast o to Authentication so that we can compare data members
		Authentication c = (Authentication) o; 

		// Compare the data members and return accordingly  
		return this.getLogin().equals(c.getLogin()) &&
				this.getPassword().equals(c.getPassword()); 
	}
}
