package ppa.labs.benchmark.clone.model.apache;

import java.io.Serializable;
import java.math.BigDecimal;

public class AddressApa implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5795122056237770694L;
	private String nameStreet;
	private Integer numStreet;
	private BigDecimal longitude;
	private BigDecimal lattitude;
	private Boolean isPrincipal;
	public String getNameStreet() {
		return nameStreet;
	}
	public void setNameStreet(String nameStreet) {
		this.nameStreet = nameStreet;
	}
	public Integer getNumStreet() {
		return numStreet;
	}
	public void setNumStreet(Integer numStreet) {
		this.numStreet = numStreet;
	}
	public BigDecimal getLongitude() {
		return longitude;
	}
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}
	public BigDecimal getLattitude() {
		return lattitude;
	}
	public void setLattitude(BigDecimal lattitude) {
		this.lattitude = lattitude;
	}
	public Boolean getIsPrincipal() {
		return isPrincipal;
	}
	public void setIsPrincipal(Boolean isPrincipal) {
		this.isPrincipal = isPrincipal;
	}

	@Override
	public String toString () {
		return (String.format(
				"\n----- obj = %s\n" +
						"nameStreet = %s\n" +
						"numStreet = %d\n" +
						"longitude = %f\n" +
						"lattitude = %f\n" +
						"isPrincipal = %s\n",
						super.toString(),
						this.getNameStreet(),
						this.getNumStreet(),
						this.getLongitude(),
						this.getLattitude(),
						String.valueOf(this.getIsPrincipal())
				));
	}
	
	@Override
	public boolean equals(Object o) { 
  
		if (o == this) { 
			return true;
		}

		/* Check if o is an instance of Address or not 
          "null instanceof [type]" also returns false */
		if (!(o instanceof AddressApa)) { 
			return false; 
		} 

		// typecast o to Address so that we can compare data members
		AddressApa c = (AddressApa) o; 

		// Compare the data members and return accordingly  
		return this.getNameStreet().equals(c.getNameStreet()) &&
				this.getNumStreet().equals(c.getNumStreet()) &&
				this.getLattitude().equals(c.getLattitude()) &&
				this.getLongitude().equals(c.getLongitude()) &&
				this.getIsPrincipal().equals(c.getIsPrincipal());
	}
}
