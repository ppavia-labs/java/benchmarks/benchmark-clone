package ppa.labs.benchmark.clone.model.constructor;

public class AuthenticationConst {

	private String login;
	private String password;
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString () {
		return (String.format(
				"\n----- obj = %s\n" +
						"login = %s\n" +
						"password = %s\n",
						super.toString(),
						this.getLogin(),
						this.getPassword()
				));
	}
	
	public AuthenticationConst duplicate() {
		AuthenticationConst auth = new AuthenticationConst();
		auth.setLogin(new String(this.login));
		auth.setPassword(new String(this.password));
		return auth;
	}
}
