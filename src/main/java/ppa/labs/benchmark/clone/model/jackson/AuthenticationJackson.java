package ppa.labs.benchmark.clone.model.jackson;

public class AuthenticationJackson {

	private String login;
	private String password;
	
	public AuthenticationJackson() {}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString () {
		return (String.format(
				"\n----- obj = %s\n" +
						"login = %s\n" +
						"password = %s\n",
						super.toString(),
						this.getLogin(),
						this.getPassword()
				));
	}
}
