package ppa.labs.benchmark.clone.model.apache;

import java.io.Serializable;
import java.util.Set;

import ppa.labs.benchmark.clone.ClonedCollectionEqualable;

public class UserApa implements Serializable, ClonedCollectionEqualable<AddressApa> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6645356689081720713L;
	private String lastName;
	private String firstName;
	private AuthenticationApa auth;
	private Set<AddressApa> addresses;
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public AuthenticationApa getAuth() {
		return auth;
	}
	public void setAuth(AuthenticationApa auth) {
		this.auth = auth;
	}	
	public Set<AddressApa> getAddresses() {
		return addresses;
	}
	public void setAddresses(Set<AddressApa> addresses) {
		this.addresses = addresses;
	}

	@Override
	public String toString () {
		return (String.format(
				"\n----- obj = %s\n" +
						"lastName = %s\n" +
						"firstName = %s\n" +
						"auth = %s\n" +
						"addresses = %s\n",
						super.toString(),
						this.getLastName(),
						this.getFirstName(),
						this.getAuth().toString(),
						this.addressesToString()
				));
	}
	
	public String addressesToString () {
		final StringBuilder out	= new StringBuilder("");
		if (this.addresses != null ) {
			this.addresses.stream()
			.forEach(addr -> {
				out.append(addr.toString()).append("\n");
			});
			return out.toString();
		}
		return "null";
	}
	@Override
	public boolean equals(Object o) { 
  
		if (o == this) { 
			return true;
		}

		/* Check if o is an instance of User or not 
          "null instanceof [type]" also returns false */
		if (!(o instanceof UserApa)) { 
			return false; 
		} 

		// typecast o to User so that we can compare data members
		UserApa c = (UserApa) o;

		// Compare the data members and return accordingly  
		if (this.getLastName().equals(c.getLastName())
				&& this.getFirstName().equals(c.getFirstName()) 
				&& this.getAuth().equals(c.getAuth()) 
				&& this.isEqualMembersInCollection(this.addresses, c.getAddresses())
				) {
			return true;
		}
		return false;
	}
}
