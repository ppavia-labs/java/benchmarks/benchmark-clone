package ppa.labs.benchmark.clone.model;

import java.util.Set;
import java.util.stream.Collectors;

import ppa.labs.benchmark.clone.ClonedCollectionEqualable;

public class User implements Cloneable, ClonedCollectionEqualable<Address> {
	private String lastName;
	private String firstName;
	private Authentication auth;
	private Set<Address> addresses;
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public Authentication getAuth() {
		return auth;
	}
	public void setAuth(Authentication auth) {
		this.auth = auth;
	}	
	public Set<Address> getAddresses() {
		return addresses;
	}
	public void setAddresses(Set<Address> addresses) {
		this.addresses = addresses;
	}
	
	@Override
	public Object clone() {
		User user = null;
		try {
	        user = (User) super.clone();
	    } catch (CloneNotSupportedException e) {
	        user = new User();
	        user.setFirstName(user.getFirstName());
	        user.setLastName(user.getLastName());
	        user.setAuth(new Authentication());
	    }
		user.auth = (Authentication) this.auth.clone();
		user.addresses = this.addresses.stream()
				.map(address -> (Address) address.clone())
				.collect(Collectors.toSet());
		return user;
	}
	
	public Object shallowClone () {
		User user = null;
		try {
	        user = (User) super.clone();
	    } catch (CloneNotSupportedException e) {
	        user = new User();
	        e.printStackTrace();
	    }
		return user;
	}
	
	@Override
	public String toString () {
		return (String.format(
				"\n----- obj = %s\n" +
						"lastName = %s\n" +
						"firstName = %s\n" +
						"auth = %s\n" +
						"addresses = %s\n",
						super.toString(),
						this.getLastName(),
						this.getFirstName(),
						this.getAuth().toString(),
						this.addressesToString()
				));
	}
	
	public String addressesToString () {
		final StringBuilder out	= new StringBuilder("");
		if (this.addresses != null ) {
			this.addresses.stream()
			.forEach(addr -> {
				out.append(addr.toString()).append("\n");
			});
			return out.toString();
		}
		return "null";
	}
 	
	@Override
	public boolean equals(Object o) { 
  
		if (o == this) { 
			return true;
		}

		/* Check if o is an instance of User or not 
          "null instanceof [type]" also returns false */
		if (!(o instanceof User)) { 
			return false; 
		} 

		// typecast o to User so that we can compare data members
		User c = (User) o;

		// Compare the data members and return accordingly  
		if (this.getLastName().equals(c.getLastName())
				&& this.getFirstName().equals(c.getFirstName()) 
				&& this.getAuth().equals(c.getAuth()) 
				&& this.isEqualMembersInCollection(this.addresses, c.getAddresses())
				) {
			return true;
		}
		return false;
	}
}
